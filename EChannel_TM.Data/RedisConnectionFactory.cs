﻿using System;
using StackExchange.Redis;

namespace EChannel_TM.Data
{
    public class RedisConnectionFactory
    {

        private static readonly Lazy<ConnectionMultiplexer> Connection;

        static RedisConnectionFactory()
        {
            var connectionString = System.Configuration.ConfigurationManager.AppSettings["RedisConnection"];

            var options = ConfigurationOptions.Parse(connectionString);

            Connection = new Lazy<ConnectionMultiplexer>(
                () => ConnectionMultiplexer.Connect(options)
            );
        }

        public static ConnectionMultiplexer GetConnection() => Connection.Value;
    }
}