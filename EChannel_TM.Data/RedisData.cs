﻿using System;
using Newtonsoft.Json;
using StackExchange.Redis;
using EChannel_TM.Model;
using System.Collections.Generic;

namespace EChannel_TM.Data
{
    public class RedisData
    {
        private static readonly string[] NullString = new string[0];
        private readonly ConnectionMultiplexer _con;
        private readonly IDatabase _db;

        public RedisData()
        {
            _con = RedisConnectionFactory.GetConnection();
            _db = _con.GetDatabase();
        }

        public string[] ReadTransactionsFromCache(string nodeName)
        {
            return RedisArrayToStringArray(_db.ListRange(nodeName));            
        }

        public bool SaveTransactionToCache(TransactionLog transactionLog)
        {
            string transactionInJson = JsonConvert.SerializeObject(transactionLog);
            try
            {                
                long listLength = _db.ListLength(transactionLog.NodeName);
                if (listLength > 10)
                {
                    _db.ListTrimAsync(transactionLog.NodeName, listLength - 9, listLength - 1);
                }
                _db.ListRightPushAsync(transactionLog.NodeName, transactionInJson);
                _db.KeyExpireAsync(transactionLog.NodeName, DateTime.Today.AddDays(1) - DateTime.Now);
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }

        }

        public List<KeyValuePair<string, int>> ReadTransactionCountFromCache()
        {
            return RedisArrayToKeyValueList(_db.HashGetAll("EChannel_TransactionLogCount"));
         }

        public bool SaveTransactionCount(string correspondentBank, string nodeName, bool success = true)
        {
            #region commentedCode
            //if (_db.HashExists("EChannel_TransactionLogCount", transactionLog.NodeName))
            //{

            // }
            // else
            // {
            //      RedisTransactionLogDM log = new RedisTransactionLogDM();
            //log.NodeName = transactionLog.NodeName;
            //    log.FailedCount = 1;
            //    log.TotalCount = 1;
            //    _db.HashSet("EChannel_TransactionLogCount", transactionLog.NodeName)
            //};


            //string transactionInJson = JsonConvert.SerializeObject(log);
            //HashEntry[] hashedMonitorReportEntry =
            //{
            //    new HashEntry( transactionLog.NodeName, transactionInJson)                
            //};
#endregion commentedCode

            try
            {
                 // _db.HashSet("EChannel_TransactionLog", hashedMonitorReportEntry);
                // _db.HashDelete("EChannel_TransactionLog")
                _db.HashIncrement("EChannel_TransactionLogCount", correspondentBank+"_"+nodeName + "_Total", 1);
                if (!success)
                {
                    _db.HashIncrement("EChannel_TransactionLogCount", correspondentBank + "_" + nodeName + "_Failed", 1);

                }
                _db.KeyExpire("EChannel_TransactionLogCount", DateTime.Today.AddDays(1) - DateTime.Now);
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        private static List<KeyValuePair<string,int>> RedisArrayToKeyValueList(HashEntry[] values)
        {
            List<KeyValuePair<string, int>> list = new List<KeyValuePair<string, int>>();
            if (values == null) return null;
            if (values.Length == 0) return null;
           foreach(HashEntry h in values)
            {
                list.Add(new KeyValuePair<string, int>(h.Name,Convert.ToInt32(h.Value)));
            }
            return list;
        }

        public string[] GetDetailsOfCorrespondentBanksAndMfBs()
        {
           return RedisArrayToStringArray(_db.HashKeys("EChannel_TransactionLogCount"));
        }

        private static string[] RedisArrayToStringArray(RedisValue[] values)
        {
            if (values == null) return null;
            if (values.Length == 0) return NullString;
            return Array.ConvertAll(values, x => (string)x);
        }

        public bool SaveLastCheckedNumber(long highestId)
        {
           return _db.StringSet("lastRetrived", highestId);
        }

        public long GetLastCheckedNumber()
        {
           RedisValue val = _db.StringGet("lastRetrived");
            if (val.HasValue) return Convert.ToInt64(val);
            return 0;
        }


    }
}