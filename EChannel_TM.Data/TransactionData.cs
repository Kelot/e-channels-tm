﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EChannel_TM.Model;
using System.Configuration;
using System.Diagnostics;
using System.Security.Permissions;
using Dapper;

namespace EChannel_TM.Data
{
    public class TransactionData
    {

        private IDbConnection db = new SqlConnection(ConfigurationManager.ConnectionStrings["E_Channel_Connection"].ConnectionString);
        private string tableName = ConfigurationManager.AppSettings["tableName"];
        private string[] codeArray = ConfigurationManager.AppSettings.Get("CodesForDown").Split(',');

        public TransactionReportVM GetCorrespondentBankCountForDuration(string mfbBank, int durationInHours)
        {

            TransactionReportVM tf = new TransactionReportVM();
            string getBankTotalCount = $@"
                                BEGIN
                                SELECT COUNT(*) FROM [EChannelsTM].[dbo].[{tableName}]
                                WHERE  RequestDate >= DATEADD(HH,-{durationInHours},GETDATE()) and NodeName = '{mfbBank}'
                                END
                                ";
            string getBankFailedCount =
                                //Bug in here 
                                $@"
                                BEGIN
                                SELECT COUNT(*) FROM [EChannelsTM].[dbo].[{tableName}]
                                WHERE  RequestDate >= DATEADD(HH,-{durationInHours},GETDATE()) and 
                                       NodeName = '{mfbBank}' and 
                                       ResponseCode IN CONCAT(',',{codeArray},',')
                                END
                                ";
            tf.MFBName = mfbBank;
            tf.Total = Convert.ToInt32(db.Query<TransactionLog>(getBankTotalCount));
            tf.Failed = Convert.ToInt32(db.Query<TransactionLog>(getBankFailedCount));
            return tf;
        }

        public async Task<Tuple<List<TransactionLog>, int>> Search(int page, int pageSize,
                                                       DateTime? endDate = null, DateTime? startDate = null, string nodeName = "",
                                                      string iso_ReceivingInstitutionID = "", string hiddenPan = "", string iso_RetrievalReference = "",
                                                      string responseCode = "", string mti = "", string isoAccount2 = "",
                                                      string isoCardAcceptorNameLocation = "", string isoTransAmount = "",
                                                      string isoAcquiringInstitutionIDCode = "", string type = "", string serviceCode = "",
                                                      string STAN = "")
        {

            string getAllQuery = $@"SELECT 
                                [NodeName],
                                [STAN],
                                [ResponseCode],
                                [HiddenPan],
                                [Iso_TransAmount],
                                [IsReversed],
                                [MessageTypeIndicator],
                                [Account2],
                                [CorrespondingBank],
                                [TransactionDuration],
                                [ResponseDate],
                                [TerminalId],
                                [Type],
                                [Iso_AcquiringInstitutionIDCode],
                                [Iso_CardAcceptorNameLocation],
                                [Iso_ReceivingInstitutionID],
                                [Iso_RetrievalReference],
                                [ServiceCode] FROM [EChannelsTM].[dbo].[{tableName}]";

            List<TransactionLog> result = (List<TransactionLog>)await db.QueryAsync<TransactionLog>(getAllQuery);

            int totalCount;
            if (!string.IsNullOrEmpty(nodeName) && !string.IsNullOrWhiteSpace(nodeName))
            {
                result = result.Where(x => x.NodeName.ToUpper().Contains(nodeName.ToUpper())).ToList();
            }
            if (!string.IsNullOrEmpty(hiddenPan) && !string.IsNullOrWhiteSpace(hiddenPan))
            {
                result = result.Where(x => x.HiddenPan.ToUpper().Contains(hiddenPan.ToUpper())).ToList();
            }
            if (!string.IsNullOrEmpty(isoTransAmount) && !string.IsNullOrWhiteSpace(isoTransAmount))
            {
                result = result.Where(x => x.Iso_TransAmount.ToUpper().Contains(isoTransAmount.ToUpper())).ToList();
            }
            if (!string.IsNullOrEmpty(mti) && !string.IsNullOrWhiteSpace(mti))
            {
                result = result.Where(x => x.MessageTypeIndicator.ToUpper().Contains(mti.ToUpper())).ToList();
            }
            if (!string.IsNullOrEmpty(responseCode) && !string.IsNullOrWhiteSpace(responseCode))
            {
                result = result.Where(x => x.ResponseCode.ToUpper().Contains(responseCode.ToUpper())).ToList();
            }

            if (!string.IsNullOrEmpty(STAN) && !string.IsNullOrWhiteSpace(STAN))
            {
                result = result.Where(x => x.STAN.ToUpper().Contains(STAN.ToUpper())).ToList();
            }
            if (!string.IsNullOrEmpty(isoAccount2) && !string.IsNullOrWhiteSpace(isoAccount2))
            {
                result = result.Where(x => x.Iso_Account2.ToUpper().Contains(isoAccount2.ToUpper())).ToList();
            }
            if (!string.IsNullOrEmpty(type) && !string.IsNullOrWhiteSpace(type))
            {
                result = result.Where(x => x.Type.ToUpper().Contains(type.ToUpper())).ToList();
            }
            else if (!string.IsNullOrEmpty(isoAcquiringInstitutionIDCode) && !string.IsNullOrWhiteSpace(isoAcquiringInstitutionIDCode))
            {
                result = result.Where(x => x.Iso_AcquiringInstitutionIDCode.ToUpper().Contains(isoAcquiringInstitutionIDCode.ToUpper())).ToList();
            }
            else if (!string.IsNullOrEmpty(isoCardAcceptorNameLocation) && !string.IsNullOrWhiteSpace(isoCardAcceptorNameLocation))
            {
                result = result.Where(x => x.Iso_CardAcceptorNameLocation.ToUpper().Contains(isoCardAcceptorNameLocation.ToUpper())).ToList();
            }
            else if (!string.IsNullOrEmpty(iso_ReceivingInstitutionID) && !string.IsNullOrWhiteSpace(iso_ReceivingInstitutionID))
            {
                result = result.Where(x => x.Iso_ReceivingInstitutionID.ToUpper().Contains(iso_ReceivingInstitutionID.ToUpper())).ToList();
            }
            else if (!string.IsNullOrEmpty(iso_RetrievalReference) && !string.IsNullOrWhiteSpace(iso_RetrievalReference))
            {
                result = result.Where(x => x.Iso_RetrievalReference.ToUpper().Contains(iso_RetrievalReference.ToUpper())).ToList();
            }
            else if (!string.IsNullOrEmpty(serviceCode) && !string.IsNullOrWhiteSpace(serviceCode))
            {
                result = result.Where(x => x.ServiceCode.ToUpper().Contains(serviceCode.ToUpper())).ToList();
            }

            else if (startDate != null && endDate != null)
            {
                result = result.Where(x => x.RequestDate >= startDate && x.RequestDate <= endDate).ToList();
            }
            else if (startDate != null || endDate != null)
            {
                DateTime searchDate = (DateTime)(startDate ?? endDate);
                result = result.Where(x => x.RequestDate.Year == searchDate.Year && x.RequestDate.Day == searchDate.Day && x.RequestDate.Month == searchDate.Month).ToList();
            }
            try
            {
                var pageNo = result.OrderByDescending(x => x.RequestDate).Skip(pageSize * (page - 1)).Take(pageSize).ToList()
                    .GroupBy(p => new { Total = result.Count() }).First();
                totalCount = pageNo.Key.Total;
                result = pageNo.Select(p => p).ToList();
            }
            catch
            {
                totalCount = 0;
            }

            return new Tuple<List<TransactionLog>, int>(result, totalCount);
        }

        public async Task<List<TransactionLog>> GetTransactionLogs(long lastIdPicked)
        {
            string selectQuery = $@"SELECT 
                                [Id],
                                [NodeName],
                                [STAN],                                
                                [ResponseCode],
                                [HiddenPan],
                                [Iso_TransAmount],
                                [IsReversed],
                                [MessageTypeIndicator],
                                [Account2],
                                [TransactionDuration],
                                [ResponseDate],
                                [TerminalId],
                                [Type],
                                [Iso_AcquiringInstitutionIDCode],
                                [Iso_CardAcceptorNameLocation],
                                [CorrespondingBank],
                                [Iso_ReceivingInstitutionID],
                                [Iso_RetrievalReference],
                                [ServiceCode] FROM [EChannelsTM].[dbo].[{tableName}]
                                WHERE Id > {lastIdPicked}";


            try
            {
                List<TransactionLog> ourCustomers = (List<TransactionLog>)await db.QueryAsync<TransactionLog>(selectQuery);
                return ourCustomers;
            }
            catch (Exception ex)
            {
                //output exception
                return null;
            }
        }


    }
}
