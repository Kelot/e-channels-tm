﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EChannel_TM.Model;
using EChannel_TM.Data;
using Newtonsoft.Json;
using System.Configuration;

namespace E_Channel_TM.Logic
{
    public class RedisLogic
    {
        private RedisData redisData = new RedisData();
        private string[] codeArray = ConfigurationManager.AppSettings.Get("CodesForDown").Split(',');
        public TransactionLog[] GetAllTransactionsPerMFB(string nodeName)
        {
            return Array.ConvertAll(
                redisData.ReadTransactionsFromCache(nodeName),
                x => JsonConvert.DeserializeObject<TransactionLog>(x));

        }

        public List<TransactionReportVM> GetCategoryStatForDurationList(int duration)
        {
            List<TransactionReportVM> statList = null;
            foreach (string t in redisData.GetDetailsOfCorrespondentBanksAndMfBs())
            {
                string mfbName = t.Split('_')[1];
                if (statList.Any(x => x.MFBName == mfbName))
                {
                    TransactionReportVM statForBank = new TransactionData().GetCorrespondentBankCountForDuration(mfbName, duration);
                    statForBank.CorrespondentBank = t.Split('_')[0];
                    statList.Add(statForBank);
                }
            };
            return statList;
        }

        public List<TransactionReportVM> ReadTransactionCount()
        {
            List<TransactionReportVM> reportList = new List<TransactionReportVM>();
            var values = redisData.ReadTransactionCountFromCache();
            if (values == null) { return null;}
            foreach (KeyValuePair<string, int> k in values)
            {
                TransactionReportVM transVM = new TransactionReportVM();
                string[] array = k.Key.Split('_');
                transVM.CorrespondentBank = array[0]; transVM.MFBName = array[1];
                if (array[2] == "Failed")
                {
                    transVM.Failed = k.Value;
                    
                        transVM.Total = values.First(x => x.Key == array[0] + '_' + array[1] + "_Total").Value;
                }
                if (array[2] == "Total")
                {
                    transVM.Total = k.Value;
                    try
                    {
                        transVM.Failed = values.First(x => x.Key == array[0] + '_' + array[1] + "_Failed").Value;
                  //      values.Remove(new KeyValuePair<string, int>(array[0] + '_' + array[1] + "_Failed", transVM.Failed));
                    }
                    catch (InvalidOperationException ex)
                    {
                        transVM.Failed = 0;
                    }
                }
                reportList.Add(transVM);

            }
            return reportList;
        }

        public void SaveTransaction(List<TransactionLog> newTransactionList)
        {
            foreach (TransactionLog t in newTransactionList)
            {
                redisData.SaveTransactionToCache(t);
                redisData.SaveTransactionCount(t.CorrespondingBank, t.NodeName, IsTransactionSucessfull(t.ResponseCode));
            }
        }

        private bool IsTransactionSucessfull(string responseCode)
        {
            return !codeArray.Contains(responseCode);
        }
    }
}
