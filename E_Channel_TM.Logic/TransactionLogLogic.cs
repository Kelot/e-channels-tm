﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EChannel_TM.Data;
using EChannel_TM.Model;
using Newtonsoft.Json;

namespace E_Channel_TM.Logic
{
    public class TransactionLogLogic
    {
        TransactionData transData = new TransactionData();
       
        public async Task<Tuple<List<TransactionLog>, int>> Search(int page, int pageSize, string transactionLogJson = "", DateTime? startDate = null, DateTime? endDate = null)
        {
            try
            {
                TransactionLog transactionLog = new TransactionLog();
                Tuple<List<TransactionLog>, int> val = new Tuple<List<TransactionLog>, int>(null, 0);
                if (!string.IsNullOrEmpty(transactionLogJson) && !string.IsNullOrWhiteSpace(transactionLogJson) &&
                        transactionLogJson != "undefined")
                {
                    transactionLog = JsonConvert.DeserializeObject<TransactionLog>(transactionLogJson);
                    return await new  TransactionData().Search(page, pageSize, startDate, endDate,
                        transactionLog.NodeName, transactionLog.Iso_ReceivingInstitutionID, transactionLog.HiddenPan,
                           transactionLog.Iso_RetrievalReference, transactionLog.ResponseCode, transactionLog.MessageTypeIndicator, transactionLog.Iso_Account2,
                           transactionLog.Iso_CardAcceptorNameLocation, transactionLog.Iso_TransAmount, transactionLog.Iso_AcquiringInstitutionIDCode,
                           transactionLog.Type, transactionLog.ServiceCode, transactionLog.STAN);

                }
                else
                {
                    return await new TransactionData().Search(page, pageSize, startDate, endDate);
                }
            }
            catch (Exception ex)
            {
                //Write Error to log file
                return null;
            }
        }

        


        //public void Save(List<TransactionLog> newTransactionList)
        //{
        //    foreach(TransactionLog t in newTransactionList)
        //    {
        //     new RedisData().SaveTransactionToCache(t);
        //    }
        //}


    }
}