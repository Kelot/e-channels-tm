﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Net.Http;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;
using System.Timers;
using EChannel_TM.Data;
using EChannel_TM.Model;
using System.Configuration;
using Newtonsoft.Json;
using System.Net.Http.Headers;

namespace EChannel_TM.DataRetriver
{
    public partial class DataRetriver : ServiceBase
    {
        private static Timer timer = new Timer();
        public DataRetriver()
        {
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {
            timer.Enabled = true;
            timer.Interval = 10000;
            timer.Elapsed += timer_Elapsed;
            timer.Start();
           
        }

        private async void timer_Elapsed(object sender, ElapsedEventArgs e)
        {
            RedisData redisData = new RedisData();
            
            timer.Stop();
            try
            {
                List<TransactionLog> transactions = await new TransactionData().GetTransactionLogs(redisData.GetLastCheckedNumber());
                Trace.WriteLine(DateTime.Now + " Gotten Transactions :" + JsonConvert.SerializeObject(transactions), "Info");
                var url = ConfigurationManager.AppSettings["ServiceUrl"];
                if (transactions.Any())
                {
                    using (var httpClient = new HttpClient())
                    {
                        httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                        //var nodeandSwitch = new Dictionary<string, string> { { "node", content } };
                        var content = new StringContent(JsonConvert.SerializeObject(transactions), Encoding.UTF8, "application/json");

                        var result = await httpClient.PostAsync(url, content);

                        if (!result.IsSuccessStatusCode)
                        {
                            Trace.WriteLine(
                                DateTime.Now + " An Error Occured during HttpRequest, StatusCode: " + result.StatusCode +
                                "Response Message: " +
                                result.ReasonPhrase, "Error while calling Web Api");

                        }
                        else
                        {
                            redisData.SaveLastCheckedNumber(transactions.OrderByDescending(x => x.Id).First().Id);
                        }
                    }
                }
            
            }
            catch (Exception ex)
            {
                Trace.WriteLine(
                            DateTime.Now + " An Error Occured in timerElasped, Message: " + ex.Message +
                            "Inner Exception: " +
                            ex.StackTrace, "Error while calling Web Api");

            }
            timer.Start();
        }

        protected override void OnStop()
        {
            Trace.WriteLine( DateTime.Now + " Service Stopped", "Info");
        }
    }
}
