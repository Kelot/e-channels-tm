﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EChannel_TM.Model
{
   public class RedisTransactionLogDM
    {
        public string  NodeName { get; set; }
        public Int64 TotalCount { get; set; }
        public Int64 FailedCount { get; set; }
    }
}
