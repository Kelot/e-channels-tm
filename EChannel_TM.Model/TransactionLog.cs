﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EChannel_TM.Model
{
    public class TransactionLog
    {
        //        [9:18:02 AM]
        //        Effiong Israel: STAN
        //[9:18:16 AM] Effiong Israel: ResponseCode
        //[9:18:20 AM] Effiong Israel: Log Date
        //[9:18:31 AM] Effiong Israel: Iso_ReceivingInstitutionID
        //[9:18:44 AM] Effiong Israel: HiddenPan
        //        Iso_Account1
        //        [9:46:03 AM] Effiong Israel: TerminalID
        //[9:46:14 AM] Effiong Israel: Iso_RetrievalReference
        //[9:46:27 AM] Effiong Israel: Iso_AcquiringInstitutionIDCode
        //[9:46:31 AM] Effiong Israel: Type
        //[9:47:13 AM] Effiong Israel: MessageTypeIndicator(for basic please)
        //[9:47:26 AM]
        //        Effiong Israel: ServiceCode(Advanced)
        //[9:48:01 AM]
        //        Effiong Israel: Iso_TransAmount(Basic also)
        //[9:48:47 AM]
        //        Effiong Israel: Iso_CardAcceptorNameLocation(Advanced)
        //[9:49:22 AM]
        //        Effiong Israel: Iso_TransactionDescription(Advanced)
        //[9:50:41 AM]
        //        EfC:\Users\Kelechi\Documents\Visual Studio 2015\Projects\EChannel_TM\EChannels_TM.Web\Controllers\TransactionLogController.csfiong Israel: Iso_Account2(advanced)
        //[9:50:54 AM]
        //        Effiong Israel: NodeName(Basic/Advanced)
        //Institution
        //Trx duration

        public long Id { get; set; }
        public string STAN { get; set; }
        public string ResponseCode { get; set; }
        public int TransactionDuration { get; set; }
        public DateTime ResponseDate { get; set; }
        public DateTime RequestDate { get; set; }
        public string Iso_ReceivingInstitutionID { get; set; }
        public string HiddenPan { get; set; }
        public string CorrespondingBank { get; set; }
        public string TerminalId { get; set; }
        public string Iso_RetrievalReference { get; set; }
        public string Iso_AcquiringInstitutionIDCode { get; set; }
        public string Type { get; set; }
        public string MessageTypeIndicator { get; set; }
        public string ServiceCode { get; set; }
        public string Iso_TransAmount { get; set; }
        public string Iso_CardAcceptorNameLocation { get; set; }
        public string Iso_Account2 { get; set; }
        public string NodeName { get; set; }
        public bool IsReversed { get; set; }

    }

    public class TotalAndFailed
    {
        public int Total { get; set; }
        public int Failed { get; set; }
    }
}
