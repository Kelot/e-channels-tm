﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EChannel_TM.Model
{
    public class TransactionReportVM
    {
        public string CorrespondentBank { get; set; }
        public string MFBName { get; set; }
        public int Total { get; set; }
        public int Failed { get; set; }
    }
}
