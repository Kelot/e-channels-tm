﻿
using System;
using System.Collections.Generic;
using System.Data;
using Newtonsoft.Json;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Http.Description;
using Microsoft.AspNetCore.Cors;
using Microsoft.Owin.Logging;
using E_Channel_TM.Logic;
using EChannel_TM.Model;

namespace EChannels_TM.Web.Controllers
{
    [EnableCors("AllowSpecificOrigin")]
    public class TransactionLogController : ApiController
    {
       // private E-Channel_Monitor.Data.TransactionContext db = new E-Channel_Monitor.Data.TransactionContext();
        private TransactionLogLogic transLogic = new TransactionLogLogic();

        public List<TotalAndFailed> GetTransactionLogs()
        {
            return transLogic.GetCategoryStat();
        }
        [HttpGet]
        [Route(("api/GetAnalysisData"))]
        public List<TotalAndFailed> GetAnalysisData()
        {
            return transLogic.GetCategoryStatforSixHoursList();
        }

        [HttpGet]
        [Route(("api/getAllTransactions"))]
        public List<TransactionLog> getAllTransactions()
        {
            return transLogic.GetCategoryStatforSixHoursList();
        }

        [ResponseType(typeof(TransactionLogDTO))]
        public async Task<IHttpActionResult> GetTransactionLog(string transactionId)
        {
            var model = await db.TransactionLogs.Select(TransactionLogDTO.SELECT).FirstOrDefaultAsync(x => x.TransactionID == transactionId);
            if (model == null)
            {
                return NotFound();
            }

            return Ok(model);
        }

        [ResponseType(typeof(TransactionLogDTO))]
        public async Task<IHttpActionResult> PostTransactionLog(string transactionLogJson)
        {
            try
            {             
                 List<TransactionLog> transactionLog = JsonConvert.DeserializeObject<List<TransactionLog>>(transactionLogJson);
               
                new TransactionHub().GetTransaction(transactionLog);
                return StatusCode(HttpStatusCode.OK);
            }
            catch (Exception ex)
            {
                Trace.WriteLine("Error","An Error occured at"+DateTime.Now+"Message: "+ ex.Message+" StackTrace"+ ex.StackTrace+" Inner Exception"+ ex.InnerException+"\n TransactionLog:"+ transactionLogJson);
                return StatusCode(HttpStatusCode.InternalServerError);
            }
        }

       


        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

       
    }
}