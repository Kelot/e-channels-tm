﻿using System;
using Microsoft.AspNet.SignalR;
using Microsoft.Owin;
using Microsoft.Owin.Cors;
using Owin;

[assembly: OwinStartup(typeof(EChannel_TM.AngWeb.Startup))]

namespace EChannel_TM.AngWeb
{
    public class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            app.Map("/signalr", map =>
            {
                map.UseCors(CorsOptions.AllowAll);
                var hubConfig = new HubConfiguration { EnableDetailedErrors = true };
                map.RunSignalR(hubConfig);
            });

        }
    }
}
