﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Web;
using Microsoft.AspNet.SignalR;
using EChannel_TM.Model;
using E_Channel_TM.Logic;
using EChannel_TM.Data;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace EChannel_TM.AngWeb
{
    public class TransactionHub: Hub
    {
        TransactionLogLogic transactionLogic = new TransactionLogLogic();
      
        RedisLogic redisLogic = new RedisLogic();
        public async Task<Boolean> GetTransaction(List<TransactionLog> newTransactionList)
        {
            try
            {
                redisLogic.SaveTransaction(newTransactionList);
                List<TransactionReportVM> transactionCountList = redisLogic.ReadTransactionCount();
                GlobalHost.ConnectionManager.GetHubContext<TransactionHub>().Clients.All.getTransaction(transactionCountList);
                return true;
            }
            catch(Exception ex)
            {
                Trace.WriteLine("Error", "An Error occured at" + DateTime.Now + "Message: " + ex.Message + " StackTrace" + ex.StackTrace + " Inner Exception" + ex.InnerException + "\n TransactionLogListt:" + JsonConvert.SerializeObject(newTransactionList));
                return false;
            }
        }

      
    }

}