import { NgModule }      from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpModule } from '@angular/http';
import { RouterModule } from '@angular/router';

import { AppComponent } from './app.component';
import { TransactionLogComponent } from './components/transaction-log/transaction-log.component';
import { SearchComponent } from './components/search/search.component';
import { AnalysisComponent } from './components/analysis/analysis.component';
import { EndPointComponent } from './components/end-point/end-point.component';

import { TransactionService } from './services/transaction.service'

@NgModule({
    imports: [
        BrowserModule,
        HttpModule,
        RouterModule.forRoot([
            {
                path: 'report',
                component: TransactionLogComponent
            },
            {
                path: 'query',
                component: SearchComponent
            },
            {
                path: 'analysis',
                component: AnalysisComponent
            },
            {
                path: '',
                redirectTo: '/report',
                pathMatch: 'full'
            }
        ])
    ],
    declarations: [AppComponent],
    providers: [TransactionService],
  bootstrap:    [ AppComponent ]
})
export class AppModule { }
