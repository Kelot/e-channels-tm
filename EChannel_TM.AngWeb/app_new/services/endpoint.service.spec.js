"use strict";
var testing_1 = require("@angular/core/testing");
var endpoint_service_1 = require("./endpoint.service");
describe('EndpointService', function () {
    beforeEach(function () {
        testing_1.TestBed.configureTestingModule({
            providers: [endpoint_service_1.EndpointService]
        });
    });
    it('should ...', testing_1.inject([endpoint_service_1.EndpointService], function (service) {
        expect(service).toBeTruthy();
    }));
});
//# sourceMappingURL=endpoint.service.spec.js.map