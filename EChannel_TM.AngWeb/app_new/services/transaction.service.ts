import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';

import 'rxjs/add/operator/toPromise';
import { Transaction } from '../classes/transaction';

@Injectable()
export class TransactionService {

    private searchTransactionsURL = "api/search";
     constructor(private http: Http) { }

     searchWithoutPaging(transactionLogInJson?: Transaction, page = 1, pageSize = 10, startDate = '', endDate = ''): Promise<Transaction[]> {
         const url = `${this.searchTransactionsURL}?page=${page}&pageSize=${pageSize}&transactionLogInJson=${JSON.stringify(transactionLogInJson)}&startDate=${startDate}&endDate=${endDate}`;

        return this.http.get(url, [page, pageSize, transactionLogInJson]).toPromise()
                       .then(response => response.json().data as Transaction[])
                       .catch(this.handleError);
      
    }

    private handleError(error: any): Promise<any> {
        console.error('An error ocurred while calling search Api')
        return Promise.reject(error.message || error);
    }

}
