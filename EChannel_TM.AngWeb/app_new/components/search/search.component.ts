import { Component, OnInit } from '@angular/core';
import { TransactionService } from '../../services/transaction.service';
import { Transaction } from '../../classes/transaction';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css']
})
export class SearchComponent implements OnInit {
    private transactionService: TransactionService;
    private transactions: Transaction;
    constructor() {
       // this.transactions: Transaction;
    }

  ngOnInit() {
      this.searchWithoutPaging();
  }

  searchWithoutPaging(transactionLogInJson?: Transaction, page?: number, pageSize?: number, startDate?: string, endDate?: string): Transaction[] {
      this.transactionService.searchWithoutPaging(transactionLogInJson, page, pageSize, startDate, endDate)
          .then(response => this.transactions = response)
          .catch(err => {
              console.log('An Error occured while trying to get all from search api due to ' + err);
              alert("Oops, Couldn't get any data due to " + err);
          });
      return null;
  }

} 
