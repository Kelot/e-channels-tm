"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require("@angular/core");
var platform_browser_1 = require("@angular/platform-browser");
var http_1 = require("@angular/http");
var router_1 = require("@angular/router");
var forms_1 = require("@angular/forms");
var ngx_bootstrap_1 = require("ngx-bootstrap");
var ngx_bootstrap_2 = require("ngx-bootstrap");
var app_component_1 = require("./app.component");
var transaction_log_component_1 = require("./components/transaction-log/transaction-log.component");
var search_component_1 = require("./components/search/search.component");
var analysis_component_1 = require("./components/analysis/analysis.component");
var mychart_component_1 = require("./directives/mychart/mychart.component");
var export_directive_1 = require("./directives/export/export.directive");
var datetime_directive_1 = require("./directives/datetime/datetime.directive");
var transaction_service_1 = require("./services/transaction/transaction.service");
var signalr_service_1 = require("./services/signalrService/signalr.service");
var broadcast_interface_1 = require("./interfaces/broadcast.interface");
var routes = [
    {
        path: 'report',
        component: transaction_log_component_1.TransactionLogComponent
    },
    {
        path: 'query',
        component: search_component_1.SearchComponent
    },
    {
        path: 'analysis',
        component: analysis_component_1.AnalysisComponent
    },
    {
        path: '',
        redirectTo: '/report',
        pathMatch: 'full'
    }
];
var AppModule = (function () {
    function AppModule() {
    }
    return AppModule;
}());
AppModule = __decorate([
    core_1.NgModule({
        imports: [
            platform_browser_1.BrowserModule,
            http_1.HttpModule,
            forms_1.FormsModule,
            ngx_bootstrap_2.TooltipModule.forRoot(),
            ngx_bootstrap_1.ModalModule.forRoot(),
            router_1.RouterModule.forRoot(routes)
        ],
        declarations: [
            app_component_1.AppComponent,
            transaction_log_component_1.TransactionLogComponent,
            search_component_1.SearchComponent,
            analysis_component_1.AnalysisComponent,
            mychart_component_1.ChartDirective,
            export_directive_1.ExportDirective,
            datetime_directive_1.DatetimeDirective
        ],
        providers: [transaction_service_1.TransactionService, signalr_service_1.SignalRService, broadcast_interface_1.Broadcaster],
        bootstrap: [app_component_1.AppComponent]
    })
], AppModule);
exports.AppModule = AppModule;
//# sourceMappingURL=app.module.js.map