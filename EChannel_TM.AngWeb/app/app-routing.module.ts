﻿import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { TransactionLogComponent } from './components/transaction-log/transaction-log.component';
import { SearchComponent } from './components/search/search.component';
import { AnalysisComponent } from './components/analysis/analysis.component';

const routes: Routes = [
    {
        path: 'report',
        component: TransactionLogComponent
    },
    {
        path: 'query',
        component: SearchComponent
    },
    {
        path: 'analysis',
        component: AnalysisComponent
    },
    {
        path: '',
        redirectTo: '/query',
        pathMatch: 'full'
    }
];

@NgModule({
    imports: [
        RouterModule.forRoot(routes)
    ],
    exports: [
        RouterModule
    ]
})

export class AppRoutingModule { }