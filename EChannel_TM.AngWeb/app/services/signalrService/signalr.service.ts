import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';

import 'rxjs/add/operator/toPromise';
import { Transaction } from '../../classes/transaction/transaction';
import { TransactionReport } from '../../classes/transactionReport/transactionReport';
import { Broadcaster} from '../../interfaces/broadcast.interface';
declare var $: any;

@Injectable()
export class SignalRService {
	
    private transactionHub : any;

    constructor(private http: Http, private broadcaster: Broadcaster) {
	    
    }

	_initialize() {
		this.transactionHub = $.connection.transactionHub;
		$.connection.hub.logging = true;
		setTimeout(() => {
			$.connection.hub.start().done(() => {
				//broadcast hubConnected
			});
		}, 200);
		 $.connection.hub.connectionSlow(()=> {
                console.log("We are currently experiencing difficulties with the connection.");
               this.broadcaster.broadcast("slowConnection");
            });

            $.connection.hub.error((error: any) =>{
                console.log("SignalR error: " + error + " occured @ " + new Date());

            });

            $.connection.hub.disconnected(()=> {
                setTimeout(()=> {
                    $.connection.hub.start().done(()=> {
                        console.log("server connected");
                        this.broadcaster.broadcast("hubConnected");
                    });
                }, 200);
            });

		this.transactionHub.client.getTransaction= (transactions: Transaction[] , stat: TransactionReport[], graphData:any)=> {
                this.broadcaster.broadcast("transactionLogs", [transactions, stat]);
            };
	}

}
