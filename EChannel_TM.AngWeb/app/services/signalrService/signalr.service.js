"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var http_1 = require("@angular/http");
require("rxjs/add/operator/toPromise");
var broadcast_interface_1 = require("../../interfaces/broadcast.interface");
var SignalRService = (function () {
    function SignalRService(http, broadcaster) {
        this.http = http;
        this.broadcaster = broadcaster;
    }
    SignalRService.prototype._initialize = function () {
        var _this = this;
        this.transactionHub = $.connection.transactionHub;
        $.connection.hub.logging = true;
        setTimeout(function () {
            $.connection.hub.start().done(function () {
                //broadcast hubConnected
            });
        }, 200);
        $.connection.hub.connectionSlow(function () {
            console.log("We are currently experiencing difficulties with the connection.");
            _this.broadcaster.broadcast("slowConnection");
        });
        $.connection.hub.error(function (error) {
            console.log("SignalR error: " + error + " occured @ " + new Date());
        });
        $.connection.hub.disconnected(function () {
            setTimeout(function () {
                $.connection.hub.start().done(function () {
                    console.log("server connected");
                    _this.broadcaster.broadcast("hubConnected");
                });
            }, 200);
        });
        this.transactionHub.client.getTransaction = function (transactions, stat, graphData) {
            _this.broadcaster.broadcast("transactionLogs", [transactions, stat]);
        };
    };
    return SignalRService;
}());
SignalRService = __decorate([
    core_1.Injectable(),
    __metadata("design:paramtypes", [http_1.Http, broadcast_interface_1.Broadcaster])
], SignalRService);
exports.SignalRService = SignalRService;
//# sourceMappingURL=signalr.service.js.map