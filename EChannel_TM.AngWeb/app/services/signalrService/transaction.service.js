"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require('@angular/core');
var http_1 = require('@angular/http');
require('rxjs/add/operator/toPromise');
var SignalRService = (function () {
    function SignalRService(http) {
        this.http = http;
        this.transactionHub = null;
        this.getTransactionForMFBURL = "http://localhost:3276/api/getTransactionsForMFB";
        this.getSummaryUrl = "http://localhost:3276/api/getSummary";
        this.analysisDataUrl = "http://localhost:3276/api/getAnalysisData";
    }
    SignalRService.prototype._initialize = function () {
        this.transactionHub = $.connection.transactionHub;
        $.connection.hub.logging = true;
        setTimeout(function () {
            $.connection.hub.start().done(function () {
                //broadcast hubConnected
            });
        }, 200);
        $.connection.hub.connectionSlow(function () {
            console.log("We are currently experiencing difficulties with the connection.");
            $rootScope.$broadcast("slowConnection");
        });
        $.connection.hub.error(function (error) {
            console.log("SignalR error: " + error + " occured @ " + new Date());
        });
        $.connection.hub.disconnected(function () {
            setTimeout(function () {
                $.connection.hub.start().done(function () {
                    console.log("server connected");
                    $rootScope.$broadcast("hubConnected");
                });
            }, 200);
        });
    };
    SignalRService.prototype.search = function (transactionLogInJson, page, pageSize, startDate, endDate) {
        if (page === void 0) { page = 1; }
        if (pageSize === void 0) { pageSize = 10; }
        if (startDate === void 0) { startDate = ''; }
        if (endDate === void 0) { endDate = ''; }
        var url = this.searchTransactionsURL + "?page=" + page + "&pageSize=" + pageSize + "&transactionLogJson=" + JSON.stringify(transactionLogInJson) + "&startDate=" + startDate + "&endDate=" + endDate;
        return this.http.get(url).toPromise()
            .then(function (response) {
            debugger;
            return Promise.resolve(response.json());
        })
            .catch(this.handleError);
    };
    SignalRService.prototype.getMFBTransaction = function (nodeName) {
        debugger;
        var url = this.getTransactionForMFBURL + "?nodeName=" + nodeName;
        return this.http.get(url).toPromise()
            .then(function (response) {
            debugger;
            Promise.resolve(response.json());
        })
            .catch(this.handleError);
    };
    SignalRService.prototype.getReportSummary = function () {
        return this.http.get(this.getSummaryUrl, this.headers).toPromise()
            .then(function (response) { return Promise.resolve(response.json()); })
            .catch(this.handleError);
    };
    SignalRService.prototype.getAnalysisData = function () {
        return this.http.get(this.analysisDataUrl).toPromise()
            .then(function (response) { return Promise.resolve(response.json()); })
            .catch(this.handleError);
    };
    SignalRService.prototype.handleError = function (error) {
        console.error('An error ocurred while calling search Api');
        return Promise.reject(error.message || error);
    };
    SignalRService = __decorate([
        core_1.Injectable(), 
        __metadata('design:paramtypes', [http_1.Http])
    ], SignalRService);
    return SignalRService;
}());
exports.SignalRService = SignalRService;
//# sourceMappingURL=transaction.service.js.map