"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var http_1 = require("@angular/http");
require("rxjs/add/operator/toPromise");
var TransactionService = (function () {
    function TransactionService(http) {
        this.http = http;
        this.headers = new http_1.Headers({
            'Content-Type': 'application/json'
        });
        this.searchTransactionsURL = "http://localhost:3276/api/search";
        this.getTransactionForMFBURL = "http://localhost:3276/api/getTransactionsForMFB";
        this.getSummaryUrl = "http://localhost:3276/api/getSummary";
        this.analysisDataUrl = "http://localhost:3276/api/getAnalysisData";
    }
    TransactionService.prototype.search = function (transactionLogInJson, page, pageSize, startDate, endDate) {
        if (page === void 0) { page = 1; }
        if (pageSize === void 0) { pageSize = 10; }
        if (startDate === void 0) { startDate = ''; }
        if (endDate === void 0) { endDate = ''; }
        var url = this.searchTransactionsURL + "?page=" + page + "&pageSize=" + pageSize + "&transactionLogJson=" + JSON.stringify(transactionLogInJson) + "&startDate=" + startDate + "&endDate=" + endDate;
        return this.http.get(url).toPromise()
            .then(function (response) {
            debugger;
            return Promise.resolve(response.json());
        })
            .catch(this.handleError);
    };
    TransactionService.prototype.getMFBTransaction = function (nodeName) {
        var url = this.getTransactionForMFBURL + "?nodeName=" + nodeName;
        return this.http.get(url).toPromise()
            .then(function (response) {
            debugger;
            return Promise.resolve(response.json());
        })
            .catch(this.handleError);
    };
    TransactionService.prototype.getReportSummary = function () {
        return this.http.get(this.getSummaryUrl, this.headers).toPromise()
            .then(function (response) { return Promise.resolve(response.json()); })
            .catch(this.handleError);
    };
    TransactionService.prototype.getAnalysisData = function (durationInHours) {
        var url = this.analysisDataUrl + '?duration=' + durationInHours;
        return this.http.get(url).toPromise()
            .then(function (response) { return Promise.resolve(response.json()); })
            .catch(this.handleError);
    };
    TransactionService.prototype.handleError = function (error) {
        console.error('An error ocurred while calling search Api');
        return Promise.reject(error.message || error);
    };
    return TransactionService;
}());
TransactionService = __decorate([
    core_1.Injectable(),
    __metadata("design:paramtypes", [http_1.Http])
], TransactionService);
exports.TransactionService = TransactionService;
//# sourceMappingURL=transaction.service.js.map