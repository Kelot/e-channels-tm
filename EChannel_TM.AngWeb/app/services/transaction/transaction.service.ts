import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';

import 'rxjs/add/operator/toPromise';
import { Transaction } from '../../classes/transaction/transaction';
import { TransactionReport } from '../../classes/transactionReport/transactionReport';

@Injectable()
export class TransactionService {
    private headers = new Headers(
        {
            'Content-Type': 'application/json'
        }
    )

    private searchTransactionsURL = "http://localhost:3276/api/search";
    private getTransactionForMFBURL = "http://localhost:3276/api/getTransactionsForMFB";
    private getSummaryUrl = "http://localhost:3276/api/getSummary";
    private analysisDataUrl = "http://localhost:3276/api/getAnalysisData";

    constructor(private http: Http) {
        
    }

    search(transactionLogInJson?: Transaction, page = 1, pageSize = 10, startDate = '', endDate = ''): Promise<any> {
        
        const url = `${this.searchTransactionsURL}?page=${page}&pageSize=${pageSize}&transactionLogJson=${JSON.stringify(transactionLogInJson)}&startDate=${startDate}&endDate=${endDate}`;

        return this.http.get(url).toPromise()
            .then(response => {
                debugger;
                return Promise.resolve(response.json());
                
            })
            .catch(this.handleError);

    }

    getMFBTransaction(nodeName: string): Promise<any> {
	const url = `${this.getTransactionForMFBURL}?nodeName=${nodeName}`;
        return this.http.get(url).toPromise()
            .then(response => {
		        debugger;
		       return Promise.resolve(response.json() as Transaction[]);
	        })
            .catch(this.handleError); 
    }

    getReportSummary(): Promise<TransactionReport[]> {
        return this.http.get(this.getSummaryUrl, this.headers).toPromise()
            .then(response => {return Promise.resolve(response.json() as TransactionReport[])})
            .catch(this.handleError);

    }

    getAnalysisData(durationInHours: number): Promise<TransactionReport[]> {
        const url = this.analysisDataUrl + '?duration=' + durationInHours;
        return this.http.get(url).toPromise()
            .then(response => Promise.resolve(response.json() as TransactionReport[]))
            .catch(this.handleError);
    }

    private handleError(error: any): Promise<any> {
        console.error('An error ocurred while calling search Api');
        return Promise.reject(error.message || error);
    }

}
