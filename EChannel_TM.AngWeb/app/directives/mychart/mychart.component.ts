import { Directive, ElementRef, Input, OnChanges , OnInit} from '@angular/core';
declare var $: any;
declare var Chart: any;

@Directive({
    selector: '[tmChart]'
})
export class ChartDirective implements OnChanges, OnInit {
  
    private doughnut: any;
    @Input() failed: number;
    @Input()  total: number;
    constructor(private el: ElementRef) {
        const green = "#45CC57";
        const red = "#fb6e52";
        // var data = {
        //     labels: [
        //         "Red",
        //         "Green"
        //     ],
        //     datasets: [
        //         {
        //             data: [10, 10],
        //             backgroundColor: [
        //                 red,
        //                 green,
        //             ],
        //             hoverBackgroundColor: [
        //                 "#FF6384",
        //                 "#36A2EB"
        //             ]
        //         }]
        // };

    var data= {
                        labels: ["MFB A", "MFB B", "MFB C"],
                        datasets: [{
                        type: 'bar',
                        label: 'Dataset 1',
                        backgroundColor: green,
                        data: [65, 0, 80],
                        }, {
                        type: 'bar',
                        label: 'Dataset 3',
                        backgroundColor: red,
                        data: [-65, 0, -80]
                        }]
                    }
        var config = {
                    type: 'bar',
                    data: {
                        labels: ["MFB A", "MFB B", "MFB C"],
                        datasets: [{
                        type: 'bar',
                        label: 'Dataset 1',
                        backgroundColor: "indianred",
                        data: [65, 0, 80],
                        }, {
                        type: 'bar',
                        label: 'Dataset 3',
                        backgroundColor: "torqoiseblue",
                        data: [-65, 0, -80]
                        }]
                    },
                    options: {
                        scales: {
                        xAxes: [{
                            stacked: true
                        }],
                        yAxes: [{
                            stacked: true
                        }]
                        }
                    }
                    };
  
        let chart = $(this.el.nativeElement)[0];
        this.doughnut = new Chart(chart, {
            type: 'bar',
            data: data,
            options: {
                scales: {
                xAxes: [{
                    stacked: true
                }],
                yAxes: [{
                    stacked: true
                }]
                }
                    }
        });
    }

    ngOnInit() {
        
              
    };

    ngOnChanges(changes: any) {
        setTimeout(() => {
            this.doughnut.chart.config.data.datasets[0].data[1] = this.failed;
            this.doughnut.chart.config.data.datasets[0].data[0] = this.total - this.failed;
            this.doughnut.update();
            this.doughnut.render(1000, true);
        },3000)
       let cFailed = changes['failed'].currentValue,
           pFailed = changes['failed'].previousValue,
           cTotal= changes['total'].currentValue,
           pTotal = changes['total'].previousValue;

       //alert('formerly we had ' + pTotal + 'as the total and ' + pFailed + ' as previous failed but now we have a new total of ' + cTotal + 'and failures of' + cFailed);
    }
    
    chartHovered() {
        console.log("Chart was hoverd");
    }
    

}
