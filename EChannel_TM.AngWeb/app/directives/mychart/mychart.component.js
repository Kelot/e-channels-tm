"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var ChartDirective = (function () {
    function ChartDirective(el) {
        this.el = el;
        var green = "#45CC57";
        var red = "#fb6e52";
        // var data = {
        //     labels: [
        //         "Red",
        //         "Green"
        //     ],
        //     datasets: [
        //         {
        //             data: [10, 10],
        //             backgroundColor: [
        //                 red,
        //                 green,
        //             ],
        //             hoverBackgroundColor: [
        //                 "#FF6384",
        //                 "#36A2EB"
        //             ]
        //         }]
        // };
        var data = {
            labels: ["MFB A", "MFB B", "MFB C"],
            datasets: [{
                    type: 'bar',
                    label: 'Dataset 1',
                    backgroundColor: green,
                    data: [65, 0, 80],
                }, {
                    type: 'bar',
                    label: 'Dataset 3',
                    backgroundColor: red,
                    data: [-65, 0, -80]
                }]
        };
        var config = {
            type: 'bar',
            data: {
                labels: ["MFB A", "MFB B", "MFB C"],
                datasets: [{
                        type: 'bar',
                        label: 'Dataset 1',
                        backgroundColor: "indianred",
                        data: [65, 0, 80],
                    }, {
                        type: 'bar',
                        label: 'Dataset 3',
                        backgroundColor: "torqoiseblue",
                        data: [-65, 0, -80]
                    }]
            },
            options: {
                scales: {
                    xAxes: [{
                            stacked: true
                        }],
                    yAxes: [{
                            stacked: true
                        }]
                }
            }
        };
        var chart = $(this.el.nativeElement)[0];
        this.doughnut = new Chart(chart, {
            type: 'bar',
            data: data,
            options: {
                scales: {
                    xAxes: [{
                            stacked: true
                        }],
                    yAxes: [{
                            stacked: true
                        }]
                }
            }
        });
    }
    ChartDirective.prototype.ngOnInit = function () {
    };
    ;
    ChartDirective.prototype.ngOnChanges = function (changes) {
        var _this = this;
        setTimeout(function () {
            _this.doughnut.chart.config.data.datasets[0].data[1] = _this.failed;
            _this.doughnut.chart.config.data.datasets[0].data[0] = _this.total - _this.failed;
            _this.doughnut.update();
            _this.doughnut.render(1000, true);
        }, 3000);
        var cFailed = changes['failed'].currentValue, pFailed = changes['failed'].previousValue, cTotal = changes['total'].currentValue, pTotal = changes['total'].previousValue;
        //alert('formerly we had ' + pTotal + 'as the total and ' + pFailed + ' as previous failed but now we have a new total of ' + cTotal + 'and failures of' + cFailed);
    };
    ChartDirective.prototype.chartHovered = function () {
        console.log("Chart was hoverd");
    };
    return ChartDirective;
}());
__decorate([
    core_1.Input(),
    __metadata("design:type", Number)
], ChartDirective.prototype, "failed", void 0);
__decorate([
    core_1.Input(),
    __metadata("design:type", Number)
], ChartDirective.prototype, "total", void 0);
ChartDirective = __decorate([
    core_1.Directive({
        selector: '[tmChart]'
    }),
    __metadata("design:paramtypes", [core_1.ElementRef])
], ChartDirective);
exports.ChartDirective = ChartDirective;
//# sourceMappingURL=mychart.component.js.map