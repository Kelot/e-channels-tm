"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var ExportDirective = (function () {
    function ExportDirective(el) {
        this.el = el;
        this.format = 'xlsx';
    }
    ExportDirective.prototype.onClick = function () {
        $(".table").tableExport({
            type: this.format,
            escape: 'false',
            pdfFontSize: 12,
            pdfLeftMargin: 6
        });
    };
    return ExportDirective;
}());
__decorate([
    core_1.Input('tmExport'),
    __metadata("design:type", String)
], ExportDirective.prototype, "format", void 0);
__decorate([
    core_1.HostListener('click'),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", []),
    __metadata("design:returntype", void 0)
], ExportDirective.prototype, "onClick", null);
ExportDirective = __decorate([
    core_1.Directive({
        selector: '[tmExport]'
    }),
    __metadata("design:paramtypes", [core_1.ElementRef])
], ExportDirective);
exports.ExportDirective = ExportDirective;
//# sourceMappingURL=export.directive.js.map