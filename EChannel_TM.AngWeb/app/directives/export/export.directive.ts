﻿import { Directive, ElementRef, HostListener, Input } from '@angular/core';
declare var $: any;

@Directive({
    selector: '[tmExport]'
})



export class ExportDirective {

    @Input('tmExport') format: string = 'xlsx';

    constructor(private el: ElementRef) { }

    @HostListener('click') onClick() {
        $(".table").tableExport({
            type: this.format,
            escape: 'false',
            pdfFontSize: 12,
            pdfLeftMargin: 6

        });
    }

}