﻿import { Directive, ElementRef, HostListener, Input } from '@angular/core';
declare var $: any
@Directive({
    selector: '[tmChart]'
})

export class ChartDirective {
    private doughnut: any;
    constructor(private el: ElementRef) {
        this.doughnut = $(el.nativeElement).find(".doughnut")[0];
    }

    @HostListener('click') onClick() {
        this.doughnut
    }

    @HostListener('hover') onHover() {

    }
}