"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var DatetimeDirective = (function () {
    function DatetimeDirective(el, ngZone) {
        var _this = this;
        this.el = el;
        this.ngZone = ngZone;
        this.ngModelChange = new core_1.EventEmitter(false);
        $(el.nativeElement).datepicker({
            autoclose: true
        }).on("changeDate", function (e) {
            debugger;
            _this.ngZone.run(function () {
                return _this.ngModelChange.emit(e.format());
            });
        });
    }
    ;
    DatetimeDirective.prototype.onChange = function (e) {
        debugger;
        //this.ngZone.run(() =>
        //    this.ngModelChange.emit(e.format()));
        //this.changeDetectorRef.detectChanges();
    };
    return DatetimeDirective;
}());
__decorate([
    core_1.Output(),
    __metadata("design:type", core_1.EventEmitter)
], DatetimeDirective.prototype, "ngModelChange", void 0);
__decorate([
    core_1.HostListener('change'),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", void 0)
], DatetimeDirective.prototype, "onChange", null);
DatetimeDirective = __decorate([
    core_1.Directive({
        selector: '[dirDatetime]'
    }),
    __metadata("design:paramtypes", [core_1.ElementRef, core_1.NgZone])
], DatetimeDirective);
exports.DatetimeDirective = DatetimeDirective;
//# sourceMappingURL=datetime.directive.js.map