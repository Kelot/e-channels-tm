﻿import { Directive, HostListener, ElementRef, Output, EventEmitter, ChangeDetectorRef, NgZone } from '@angular/core';
declare var $: any;

@Directive({
    selector: '[dirDatetime]'
})

export class DatetimeDirective {
    @Output() ngModelChange: EventEmitter<any> = new EventEmitter(false);
   

    private changeDetectorRef: ChangeDetectorRef;

    constructor(private el: ElementRef, private ngZone: NgZone) {
        $(el.nativeElement).datepicker({
            autoclose: true
        }).on("changeDate", (e: any) => {
            debugger;
            this.ngZone.run(() =>
                this.ngModelChange.emit(e.format()));
        });
    };


    @HostListener('change') onChange(e: any) {
        debugger;
    //this.ngZone.run(() =>
    //    this.ngModelChange.emit(e.format()));
    //this.changeDetectorRef.detectChanges();
}

   
}