"use strict";
var testing_1 = require("@angular/core/testing");
var end_point_component_1 = require("./end-point.component");
describe('EndPointComponent', function () {
    var component;
    var fixture;
    beforeEach(testing_1.async(function () {
        testing_1.TestBed.configureTestingModule({
            declarations: [end_point_component_1.EndPointComponent]
        })
            .compileComponents();
    }));
    beforeEach(function () {
        fixture = testing_1.TestBed.createComponent(end_point_component_1.EndPointComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });
    it('should create', function () {
        expect(component).toBeTruthy();
    });
});
//# sourceMappingURL=end-point.component.spec.js.map