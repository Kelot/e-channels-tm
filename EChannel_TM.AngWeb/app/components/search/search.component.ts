import { Component, OnInit, Input } from '@angular/core';
import { TransactionService } from '../../services/transaction/transaction.service';
import { Transaction } from '../../classes/transaction/transaction';
declare var $: any

@Component({
  selector: 'app-search',
  templateUrl: 'app/components/search/search.component.html',
  styleUrls: ['app/components/search/search.component.css'],
  providers: [TransactionService]
})
    
export class SearchComponent implements OnInit {
       
    
    private transactions: Transaction[];

    page: number = 1;
    pageNumber: number =1;
    pageSize: number = 10;
    transactionLog= new Transaction;
    isBasic: boolean = true;
    presentPage: number = 1;
    entries: number;
    max: number;
    

    constructor(private transactionService: TransactionService) {
       
    }

    ngOnInit() {
        $("[type='number']").keypress((evt: any)=> {
            evt.preventDefault();
        });

        this.search();
        console.log(this.isBasic);
    }

    toggleSearch(): void {
        this.isBasic = !this.isBasic;
    };

    search(transactionLogInJson?: Transaction, page?: number, pageSize?: number, startDate?: string, endDate?: string): void {
        debugger;
      this.transactionService.search(transactionLogInJson, page, pageSize, startDate, endDate)
          .then(response => {
		    
              this.transactions = response.m_Item1;
              this.entries = response.m_Item2;
               let maximum = this.entries / this.pageSize;
             
              if ((this.entries % this.pageSize) !== 0) {
                  maximum += 1;
              }
             this.max = maximum;
          })
          .catch(err => {
              console.log('An Error occured while trying to get all from search api due to ' + err);
              alert("Oops, Couldn't get any data due to " + err);
          });
      
  }

} 
