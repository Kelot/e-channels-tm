"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var transaction_service_1 = require("../../services/transaction/transaction.service");
var transaction_1 = require("../../classes/transaction/transaction");
var SearchComponent = (function () {
    function SearchComponent(transactionService) {
        this.transactionService = transactionService;
        this.page = 1;
        this.pageNumber = 1;
        this.pageSize = 10;
        this.transactionLog = new transaction_1.Transaction;
        this.isBasic = true;
        this.presentPage = 1;
    }
    SearchComponent.prototype.ngOnInit = function () {
        $("[type='number']").keypress(function (evt) {
            evt.preventDefault();
        });
        this.search();
        console.log(this.isBasic);
    };
    SearchComponent.prototype.toggleSearch = function () {
        this.isBasic = !this.isBasic;
    };
    ;
    SearchComponent.prototype.search = function (transactionLogInJson, page, pageSize, startDate, endDate) {
        var _this = this;
        debugger;
        this.transactionService.search(transactionLogInJson, page, pageSize, startDate, endDate)
            .then(function (response) {
            _this.transactions = response.m_Item1;
            _this.entries = response.m_Item2;
            var maximum = _this.entries / _this.pageSize;
            if ((_this.entries % _this.pageSize) !== 0) {
                maximum += 1;
            }
            _this.max = maximum;
        })
            .catch(function (err) {
            console.log('An Error occured while trying to get all from search api due to ' + err);
            alert("Oops, Couldn't get any data due to " + err);
        });
    };
    return SearchComponent;
}());
SearchComponent = __decorate([
    core_1.Component({
        selector: 'app-search',
        templateUrl: 'app/components/search/search.component.html',
        styleUrls: ['app/components/search/search.component.css'],
        providers: [transaction_service_1.TransactionService]
    }),
    __metadata("design:paramtypes", [transaction_service_1.TransactionService])
], SearchComponent);
exports.SearchComponent = SearchComponent;
//# sourceMappingURL=search.component.js.map