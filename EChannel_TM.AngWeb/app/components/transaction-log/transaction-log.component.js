"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var report_1 = require("../../classes/report/report");
var transaction_service_1 = require("../../services/transaction/transaction.service");
var broadcast_interface_1 = require("../../interfaces/broadcast.interface");
var TransactionLogComponent = (function () {
    function TransactionLogComponent(transactionService, broadcaster, changes) {
        this.transactionService = transactionService;
        this.broadcaster = broadcaster;
        this.changes = changes;
        this.lastMFBTransactions = [];
        this.newTransReport = [];
        this.reportMap = new Map();
    }
    TransactionLogComponent.prototype.ngOnInit = function () {
        this.getReportSummary();
    };
    TransactionLogComponent.prototype.pullTransactions = function (nodeName) {
        var _this = this;
        this.transactionService.getMFBTransaction(nodeName)
            .then(function (result) { _this.lastMFBTransactions = result; console.log(_this.lastMFBTransactions); })
            .catch(function (error) { return alert('Couldnt get recent transactions'); });
    };
    TransactionLogComponent.prototype.onTransactionReceived = function () {
        var _this = this;
        this.broadcaster.on('transactionLog').subscribe(function (newTransactionList) { return _this.populateList(newTransactionList); });
    };
    TransactionLogComponent.prototype.mfbClicked = function (mfbName) {
        this.pullTransactions(mfbName);
        console.log(this.lastMFBTransactions);
    };
    TransactionLogComponent.prototype.getReportSummary = function () {
        var _this = this;
        this.transactionService.getReportSummary()
            .then(function (data) { return _this.populateList(data); });
    };
    TransactionLogComponent.prototype.populateList = function (data) {
        var _this = this;
        data.forEach(function (transReport) {
            var newReport = _this.reportMap.get(transReport.CorrespondentBank);
            if (newReport == null) {
                newReport = new report_1.Report();
                newReport.name = transReport.CorrespondentBank;
                newReport.mfbs = _this.newTransReport;
            }
            newReport.mfbs = data.filter(function (x) { return x.CorrespondentBank === newReport.name; });
            newReport.failed += transReport.Failed;
            newReport.total += transReport.Total;
            _this.reportMap.set(transReport.CorrespondentBank, newReport);
        });
        this.bankNames = this.convertIteratorToArray(this.reportMap.keys());
        console.log(this.bankNames);
    };
    TransactionLogComponent.prototype.convertIteratorToArray = function (iterator) {
        var arr = new Array();
        var isDone = false;
        while (!isDone) {
            var obj = iterator.next();
            arr.push(obj.value);
            isDone = obj.done;
        }
        arr.pop();
        return arr;
    };
    return TransactionLogComponent;
}());
TransactionLogComponent = __decorate([
    core_1.Component({
        selector: 'app-transaction-log',
        templateUrl: 'app/components/transaction-log/transaction-log.component.html',
        styleUrls: ['app/components/transaction-log/transaction-log.component.css']
    }),
    __metadata("design:paramtypes", [transaction_service_1.TransactionService, broadcast_interface_1.Broadcaster, core_1.ChangeDetectorRef])
], TransactionLogComponent);
exports.TransactionLogComponent = TransactionLogComponent;
//# sourceMappingURL=transaction-log.component.js.map