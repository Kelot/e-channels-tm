import { Component, OnInit, ChangeDetectorRef, ViewChild, Input } from '@angular/core';
import { Report } from '../../classes/report/report';
import { Transaction } from '../../classes/transaction/transaction';
import { TransactionReport } from '../../classes/transactionReport/transactionReport';
import { TransactionService } from '../../services/transaction/transaction.service';
import { Broadcaster } from '../../interfaces/broadcast.interface';


@Component({
	selector: 'app-transaction-log',
	templateUrl: 'app/components/transaction-log/transaction-log.component.html',
	styleUrls: ['app/components/transaction-log/transaction-log.component.css']
})
export class TransactionLogComponent implements OnInit {
	
   
	private report: Report[];
	private lastMFBTransactions: Transaction[] = [];
	private newTransReport: TransactionReport[] = [];
	private reportMap: Map<string, Report> = new Map<string, Report>();
	private bankNames: string[];

	constructor(private transactionService: TransactionService, private broadcaster:Broadcaster, private changes: ChangeDetectorRef) {}

	ngOnInit() {
		this.getReportSummary();
	}

	public pullTransactions(nodeName: string) {
        this.transactionService.getMFBTransaction(nodeName)
            .then(result => {this.lastMFBTransactions = result;console.log(this.lastMFBTransactions)})
            .catch(error => alert('Couldnt get recent transactions'));
    }

    onTransactionReceived(){
		this.broadcaster.on('transactionLog').subscribe(newTransactionList =>this.populateList(newTransactionList as TransactionReport[]));
	}

	public mfbClicked(mfbName: string): void {
		
		this.pullTransactions(mfbName);
		console.log(this.lastMFBTransactions);
	}

	getReportSummary(): void {

		this.transactionService.getReportSummary()
			.then(data => this.populateList(data));
	}

	populateList(data : TransactionReport[]): void{
				data.forEach(transReport => {
					
					let newReport = this.reportMap.get(transReport.CorrespondentBank);
					if(newReport == null){
						newReport = new Report();
						newReport.name = transReport.CorrespondentBank;
						newReport.mfbs = this.newTransReport;
					}
					
					newReport.mfbs = data.filter(x => x.CorrespondentBank === newReport.name);
					newReport.failed += transReport.Failed;
					newReport.total += transReport.Total;
					this.reportMap.set(transReport.CorrespondentBank, newReport)

				});
				this.bankNames = this.convertIteratorToArray(this.reportMap.keys());
				console.log(this.bankNames);
	}

	convertIteratorToArray(iterator: IterableIterator<string>): string[] {
		let arr = new Array<string>();
		let isDone = false;
		while (!isDone) {
			let obj = iterator.next();
			arr.push(obj.value);
			isDone = obj.done;
		}
		arr.pop();
		return arr;

	}

 
}

