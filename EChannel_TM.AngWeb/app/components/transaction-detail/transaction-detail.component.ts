﻿import { Component, ViewChild, Input} from '@angular/core';
import { TransactionService } from '../../services/transaction/transaction.service';
import { Transaction } from '../../classes/transaction/transaction';
import { ModalDirective } from 'ngx-bootstrap';

@Component ({
    selector: 'transaction-detail',
    templateUrl: 'app/components/transaction-detail/transaction-detail.html',
    styleUrls: ['app/components/transaction-detail/transaction-detail.css']
})

export class TransactionDetailComponent {
    @ViewChild('childModal') public childModal: ModalDirective;
    @Input() mfbName: string;
    private lastMFBTransactions: Transaction[];
    private transService: TransactionService;

    ngOnInit() {
	    debugger;
        this.pullTransactions(this.mfbName);
    }

    public pullTransactions(nodeName: string) {
        this.transService.getMFBTransaction(nodeName)
            .then(result => this.lastMFBTransactions = result)
            .catch(error => alert('Couldnt get recent transactions'));
    }
    public showChildModal(): void {
        this.childModal.show();
    }
	public hideChildModal():void {
    this.childModal.hide();
  }

}