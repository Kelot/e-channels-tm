"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var ngx_bootstrap_1 = require("ngx-bootstrap");
var TransactionDetailComponent = (function () {
    function TransactionDetailComponent() {
    }
    TransactionDetailComponent.prototype.ngOnInit = function () {
        debugger;
        this.pullTransactions(this.mfbName);
    };
    TransactionDetailComponent.prototype.pullTransactions = function (nodeName) {
        var _this = this;
        this.transService.getMFBTransaction(nodeName)
            .then(function (result) { return _this.lastMFBTransactions = result; })
            .catch(function (error) { return alert('Couldnt get recent transactions'); });
    };
    TransactionDetailComponent.prototype.showChildModal = function () {
        this.childModal.show();
    };
    TransactionDetailComponent.prototype.hideChildModal = function () {
        this.childModal.hide();
    };
    return TransactionDetailComponent;
}());
__decorate([
    core_1.ViewChild('childModal'),
    __metadata("design:type", ngx_bootstrap_1.ModalDirective)
], TransactionDetailComponent.prototype, "childModal", void 0);
__decorate([
    core_1.Input(),
    __metadata("design:type", String)
], TransactionDetailComponent.prototype, "mfbName", void 0);
TransactionDetailComponent = __decorate([
    core_1.Component({
        selector: 'transaction-detail',
        templateUrl: 'app/components/transaction-detail/transaction-detail.html',
        styleUrls: ['app/components/transaction-detail/transaction-detail.css']
    })
], TransactionDetailComponent);
exports.TransactionDetailComponent = TransactionDetailComponent;
//# sourceMappingURL=transaction-detail.component.js.map