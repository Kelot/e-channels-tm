"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var mfbCount_1 = require("../../classes/mfbCount/mfbCount");
var transaction_service_1 = require("../../services/transaction/transaction.service");
var broadcast_interface_1 = require("../../interfaces/broadcast.interface");
var AnalysisComponent = (function () {
    function AnalysisComponent(transactionService, broadcaster) {
        this.transactionService = transactionService;
        this.broadcaster = broadcaster;
        this.commercialbankToMfbCountsMap = new Map();
    }
    AnalysisComponent.prototype.OnInit = function () {
        var _this = this;
        this.transactionService.getAnalysisData(6)
            .then(function (response) { return _this.populateMap(response); })
            .catch(function (error) { return alert("An Error Occured due to " + error); });
    };
    AnalysisComponent.prototype.populateMap = function (data) {
        var _this = this;
        var mfbSuccessCount = new mfbCount_1.MfbCountObj();
        var mfbFailCount = new mfbCount_1.MfbCountObj();
        var mfbCountList = new Array();
        data.forEach(function (transReport) {
            mfbCountList = _this.commercialbankToMfbCountsMap.get(transReport.CorrespondentBank);
            mfbSuccessCount.count = transReport.Total - transReport.Failed;
            mfbSuccessCount.mfbName = mfbFailCount.mfbName = transReport.MFBName;
            mfbFailCount.count = transReport.Failed;
            mfbCountList = mfbCountList.concat([mfbSuccessCount, mfbFailCount]);
            _this.commercialbankToMfbCountsMap.set(transReport.CorrespondentBank);
        });
        this.bankNames = this.convertIteratorToArray(this.commercialbankToMfbCountsMap.keys());
        console.log(this.bankNames);
    };
    AnalysisComponent.prototype.convertIteratorToArray = function (iterator) {
        var arr = new Array();
        var isDone = false;
        while (!isDone) {
            var obj = iterator.next();
            arr.push(obj.value);
            isDone = obj.done;
        }
        arr.pop();
        return arr;
    };
    // events
    AnalysisComponent.prototype.chartClicked = function (e) {
        console.log(e);
    };
    AnalysisComponent.prototype.chartHovered = function (e) {
        console.log(e);
    };
    AnalysisComponent.prototype.ngOnInit = function () {
    };
    return AnalysisComponent;
}());
__decorate([
    core_1.Input(),
    __metadata("design:type", Object)
], AnalysisComponent.prototype, "failed", void 0);
__decorate([
    core_1.Input(),
    __metadata("design:type", Object)
], AnalysisComponent.prototype, "success", void 0);
AnalysisComponent = __decorate([
    core_1.Component({
        selector: 'app-analysis',
        templateUrl: 'app/components/analysis/analysis.component.html',
        styleUrls: ['app/components/analysis/analysis.component.css']
    }),
    __metadata("design:paramtypes", [transaction_service_1.TransactionService, broadcast_interface_1.Broadcaster])
], AnalysisComponent);
exports.AnalysisComponent = AnalysisComponent;
//# sourceMappingURL=analysis.component.js.map