import { Component, Input, OnInit } from '@angular/core';
import { MfbCountObj } from '../../classes/mfbCount/mfbCount';

import { TransactionReport } from '../../classes/transactionReport/transactionReport';
import { TransactionService } from '../../services/transaction/transaction.service';
import { Broadcaster } from '../../interfaces/broadcast.interface';

@Component({
  selector: 'app-analysis',
  templateUrl: 'app/components/analysis/analysis.component.html',
  styleUrls: ['app/components/analysis/analysis.component.css']
})

export class AnalysisComponent implements OnInit {
    @Input() failed:any;
    @Input() success:any;
    private commercialbankToMfbCountsMap: Map<string, MfbCountObj[]> = new Map<string, MfbCountObj[]>();    
    private bankNames: string[];

    constructor(private transactionService: TransactionService, private broadcaster: Broadcaster) { }

    OnInit() {
        this.transactionService.getAnalysisData(6)
            .then(response => this.populateMap(response as TransactionReport[]))
            .catch(error => alert("An Error Occured due to "+error))
            
    }

    populateMap(data: TransactionReport[]) {
        let mfbSuccessCount = new MfbCountObj();
        let mfbFailCount = new MfbCountObj();
        let mfbCountList = new Array<MfbCountObj>();

        data.forEach(transReport => {

            mfbCountList = this.commercialbankToMfbCountsMap.get(transReport.CorrespondentBank);
            mfbSuccessCount.count = transReport.Total - transReport.Failed;
            mfbSuccessCount.mfbName = mfbFailCount.mfbName = transReport.MFBName;
            mfbFailCount.count = transReport.Failed;
            mfbCountList = mfbCountList.concat([mfbSuccessCount, mfbFailCount]);
            this.commercialbankToMfbCountsMap.set(transReport.CorrespondentBank, )
            
        });
        this.bankNames = this.convertIteratorToArray(this.commercialbankToMfbCountsMap.keys());
        console.log(this.bankNames);       


    }

    convertIteratorToArray(iterator: IterableIterator<string>): string[] {
        let arr = new Array<string>();
        let isDone = false;
        while (!isDone) {
            let obj = iterator.next();
            arr.push(obj.value);
            isDone = obj.done;
        }
        arr.pop();
        return arr;

    }
    // events
    public chartClicked(e: any): void {
        console.log(e);
    }

    public chartHovered(e: any): void {
        console.log(e);
    }

  ngOnInit() {
  }

}
