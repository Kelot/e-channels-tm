export class Transaction { 
    Id: number;     
    ResponseCode : string;
    STAN: string ;
    TransactionDuration: number ;
    ResponseDate: Date;
    RequestDate : Date;
    Iso_ReceivingInstitutionID: string;
    HiddenPan: string;
    CorrespondingBank :string;
    TerminalId: string;
    Iso_RetrievalReference: string ;
    Iso_AcquiringInstitutionIDCode: string ;
    Type: string ;
    MessageTypeIndicator: string ;
    ServiceCode: string ;
    Iso_TransAmount: string ;
    Iso_CardAcceptorNameLocation: string ;
    Iso_Account2: string ;
    NodeName: string;
    IsReversed: boolean;
}
