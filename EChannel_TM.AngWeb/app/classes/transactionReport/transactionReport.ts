﻿export class TransactionReport {
    CorrespondentBank: string;
    MFBName: string;
    Total: number = 0;
    Failed: number = 0;
}