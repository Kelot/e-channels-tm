﻿import {TransactionReport} from '../transactionReport/transactionReport';
export class Report {
    public name: string;
    total: number = 0;
    failed: number = 0;
    mfbs: TransactionReport[];
}