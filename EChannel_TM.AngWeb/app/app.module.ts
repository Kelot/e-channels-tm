import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpModule } from '@angular/http';
import { RouterModule, Routes } from '@angular/router';
import { FormsModule } from '@angular/forms';
import { ModalModule } from 'ngx-bootstrap';
import { TooltipModule } from 'ngx-bootstrap';

import { AppComponent } from './app.component';
import { TransactionLogComponent } from './components/transaction-log/transaction-log.component';
import { SearchComponent } from './components/search/search.component';
import { AnalysisComponent } from './components/analysis/analysis.component';
import { ChartDirective } from './directives/mychart/mychart.component';
import { ExportDirective } from './directives/export/export.directive';
import { DatetimeDirective } from './directives/datetime/datetime.directive';

import { TransactionService } from './services/transaction/transaction.service';
import { SignalRService } from './services/signalrService/signalr.service';
import {Broadcaster} from './interfaces/broadcast.interface';
const routes: Routes = [
    {
        path: 'report',
        component: TransactionLogComponent
    },
    {
        path: 'query',
        component: SearchComponent
    },
    {
        path: 'analysis',
        component: AnalysisComponent
    },
    {
        path: '',
        redirectTo: '/report',
        pathMatch: 'full'
    }
]
@NgModule({
    imports: [
        BrowserModule,
        HttpModule,
        FormsModule,
        TooltipModule.forRoot(),
        ModalModule.forRoot(),
        RouterModule.forRoot(routes)
    ],
    declarations: [
        AppComponent,
        TransactionLogComponent,
        SearchComponent,
        AnalysisComponent,
        ChartDirective,
        ExportDirective,
        DatetimeDirective
    ],
    providers: [TransactionService, SignalRService, Broadcaster],
    bootstrap: [AppComponent]
})
export class AppModule { }
