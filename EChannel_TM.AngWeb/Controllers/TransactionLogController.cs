﻿using System;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json;
using System.Diagnostics;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Http.Description;
using E_Channel_TM.Logic;
using EChannel_TM.Model;

namespace EChannel_TM.AngWeb.Controllers
{
    [RoutePrefix("api")]
    public class TransactionLogController : ApiController
    {
        private TransactionLogLogic transLogic = new TransactionLogLogic();
        private RedisLogic redisLogic = new RedisLogic();


        //public List<TotalAndFailed> GetTransactionLogs()
        //{
        //    return transLogic.GetCategoryStat();
        //}

        [HttpGet]
        [Route("getAnalysisData")]
        public List<TransactionReportVM> GetAnalysisData(int duration)
        {
            return redisLogic.GetCategoryStatForDurationList(duration);
        }

        [HttpGet]
        [Route("search")]
        public async Task<Tuple<List<TransactionLog>, int>> SearchTransactionLog(int page, int pageSize, string transactionLogJson = "",
                                                                                 DateTime? startDate = null, DateTime? endDate = null)
        {
            return await transLogic.Search(page, pageSize, transactionLogJson, startDate, endDate);
        }

        [HttpGet]
        [Route("getTransactionsForMFB")]
        public TransactionLog[] GetAllTransactions(string nodeName)
        {
            return redisLogic.GetAllTransactionsPerMFB(nodeName);
        }

        [HttpGet]
        [Route("getSummary")]
        public List<TransactionReportVM> GetTransactionCount()
        {
            return redisLogic.ReadTransactionCount();
        }

        [HttpPost]
        [Route("transactionLog")]
        [ResponseType(typeof(TransactionLog))]
        public async Task<IHttpActionResult> PostTransactionLog(List<TransactionLog> transactionLog)
        {
            try
            {
                await new TransactionHub().GetTransaction(transactionLog);
                return StatusCode(HttpStatusCode.OK);
            }
            catch (Exception ex)
            {
                Trace.WriteLine("Error", "An Error occured at" + DateTime.Now + "Message: " + ex.Message + " StackTrace" + ex.StackTrace + " Inner Exception" + ex.InnerException + "\n TransactionLog:" + JsonConvert.SerializeObject(transactionLog));
                return StatusCode(HttpStatusCode.InternalServerError);
            }
        }
    }
}
