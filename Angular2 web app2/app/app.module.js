"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var core_1 = require("@angular/core");
var platform_browser_1 = require("@angular/platform-browser");
var http_1 = require("@angular/http");
var router_1 = require("@angular/router");
var app_component_1 = require("./app.component");
var transaction_log_component_1 = require("./components/transaction-log/transaction-log.component");
var search_component_1 = require("./components/search/search.component");
var analysis_component_1 = require("./components/analysis/analysis.component");
var transaction_service_1 = require("./services/transaction.service");
var AppModule = (function () {
    function AppModule() {
    }
    return AppModule;
}());
AppModule = __decorate([
    core_1.NgModule({
        imports: [
            platform_browser_1.BrowserModule,
            http_1.HttpModule,
            router_1.RouterModule.forRoot([
                {
                    path: 'report',
                    component: transaction_log_component_1.TransactionLogComponent
                },
                {
                    path: 'query',
                    component: search_component_1.SearchComponent
                },
                {
                    path: 'analysis',
                    component: analysis_component_1.AnalysisComponent
                },
                {
                    path: '',
                    redirectTo: '/report',
                    pathMatch: 'full'
                }
            ])
        ],
        declarations: [app_component_1.AppComponent],
        providers: [transaction_service_1.TransactionService],
        bootstrap: [app_component_1.AppComponent]
    })
], AppModule);
exports.AppModule = AppModule;
//# sourceMappingURL=app.module.js.map