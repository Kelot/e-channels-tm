"use strict";
var testing_1 = require("@angular/core/testing");
var analysis_component_1 = require("./analysis.component");
describe('AnalysisComponent', function () {
    var component;
    var fixture;
    beforeEach(testing_1.async(function () {
        testing_1.TestBed.configureTestingModule({
            declarations: [analysis_component_1.AnalysisComponent]
        })
            .compileComponents();
    }));
    beforeEach(function () {
        fixture = testing_1.TestBed.createComponent(analysis_component_1.AnalysisComponent);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });
    it('should create', function () {
        expect(component).toBeTruthy();
    });
});
//# sourceMappingURL=analysis.component.spec.js.map