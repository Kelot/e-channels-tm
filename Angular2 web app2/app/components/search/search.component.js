"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var core_1 = require("@angular/core");
var SearchComponent = (function () {
    function SearchComponent() {
        // this.transactions: Transaction;
    }
    SearchComponent.prototype.ngOnInit = function () {
        this.searchWithoutPaging();
    };
    SearchComponent.prototype.searchWithoutPaging = function (transactionLogInJson, page, pageSize, startDate, endDate) {
        var _this = this;
        this.transactionService.searchWithoutPaging(transactionLogInJson, page, pageSize, startDate, endDate)
            .then(function (response) { return _this.transactions = response; })
            .catch(function (err) {
            console.log('An Error occured while trying to get all from search api due to ' + err);
            alert("Oops, Couldn't get any data due to " + err);
        });
        return null;
    };
    return SearchComponent;
}());
SearchComponent = __decorate([
    core_1.Component({
        selector: 'app-search',
        templateUrl: './search.component.html',
        styleUrls: ['./search.component.css']
    }),
    __metadata("design:paramtypes", [])
], SearchComponent);
exports.SearchComponent = SearchComponent;
//# sourceMappingURL=search.component.js.map